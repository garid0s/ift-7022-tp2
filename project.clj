(defproject ift-7022-tp2 "0.1.0-SNAPSHOT"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [incanter "1.5.6"]
                 [org.apache.lucene/lucene-core "5.3.1"]
                 [org.apache.lucene/lucene-queryparser "5.3.1"]
                 [org.apache.lucene/lucene-analyzers-common "5.3.1"]
                 [org.apache.lucene/lucene-highlighter "5.3.1"]]
  :main ^:skip-aot ift-7022-tp2.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
