(ns ift-7022-tp2.lucene-wrapper
  "Copied from https://github.com/kostafey/clucy. I copied the code to add the tf idf feature. I'll do a pull request later."
  (:import (java.io StringReader)
           (java.nio.file Paths)
           (java.net URI)
           (org.apache.lucene.analysis Analyzer TokenStream)
           (org.apache.lucene.analysis.standard StandardAnalyzer)
           (org.apache.lucene.document Document Field FieldType
                                       Field$Index Field$Store Field$TermVector)
           (org.apache.lucene.index IndexWriter IndexReader Term
                                    IndexWriterConfig DirectoryReader FieldInfo
                                    IndexOptions)
           (org.apache.lucene.queryparser.classic QueryParser)
           (org.apache.lucene.search BooleanClause BooleanClause$Occur
                                     BooleanQuery IndexSearcher Query ScoreDoc
                                     Scorer TermQuery)
           (org.apache.lucene.search.highlight Highlighter QueryScorer
                                               SimpleHTMLFormatter)
           (org.apache.lucene.util Version AttributeSource)
           (org.apache.lucene.store NIOFSDirectory RAMDirectory Directory)
           (org.apache.lucene.search.similarities DefaultSimilarity)))

(def ^{:dynamic true} *version* Version/LUCENE_CURRENT)
(def ^{:dynamic true} *analyzer* (StandardAnalyzer.))
(def ^{:dynamic true} *similarity* (DefaultSimilarity.))

;; To avoid a dependency on either contrib or 1.2+
(defn as-str ^String [x]
  (cond
    (keyword? x) (name x)
    :default (str x)))

(defn reader? [arg]
  (instance? java.io.Reader arg))

(defn file? [arg]
  (instance? java.io.File arg))

(defn memory-index
  "Create a new index in RAM."
  []
  (RAMDirectory.))

(defn disk-index
  "Create a new index in a directory on disk."
  [^String dir-path]
  (NIOFSDirectory.
   (Paths/get (URI. (str "file:///" dir-path)))))

(defn index-writer
  "Create an IndexWriter."
  ^IndexWriter
  [index]
  (IndexWriter. index
                (doto (IndexWriterConfig. *analyzer*)
                  (.setSimilarity *similarity*))))

(defn- index-reader
  "Create an IndexReader."
  ^IndexReader
  [index]
  (DirectoryReader/open ^Directory index))

(defn set-field-params [item params]
  (with-meta
    (if (instance? clojure.lang.IObj item)
      item
      #(identity item)) params))

(defn make-field-type [meta-map]
  (doto (FieldType.)
    (.setIndexOptions (if (get meta-map :indexed true)
                        IndexOptions/DOCS
                        IndexOptions/NONE))
    (.setStored    (boolean (get meta-map :stored true)))))
    ;(.setOmitNorms (not (boolean (get meta-map :norms false))))
    ;(.setStoreTermVectors       (boolean
    ;                             (get meta-map :positions-offsets false)))
    ;(.setStoreTermVectorOffsets (boolean
    ;                             (get meta-map :positions-offsets false)))))


(defn add-field
  "Add a Field to a Document.
  Following options are allowed for meta-map:
  :stored  - when false, then do not store the field value in the index.
  :indexed - when false, then do not index the field.
  :norms   - when :indexed is enabled user this option to disable/enable
             the storing of norms.
  :positions-offsets - when true store token positions into the term vector
                       for this field and field's indexed form should be also
                       stored into term vectors."
  ([document key value]
   (add-field document key value {}))

  ([document key value meta-map]
   (.add ^Document document
         (Field. (as-str key)
                 value
                 (make-field-type meta-map)))))

(defn- map-stored
  "Returns a hash-map containing all of the values in the map that
  will be stored in the search index."
  [map-in]
  (merge {}
         (filter (complement nil?)
                 (map (fn [item]
                        (if (or (= nil (meta map-in))
                                (not= false
                                      (:stored ((first item) (meta map-in)))))
                          item)) map-in))))

(defn- concat-values
  "Concatenate all the maps values being stored into a single string."
  [map-in]
  (apply str (interpose " " (vals (map-stored map-in)))))

(defn- map->document
  "Create a Document from a map."
  [map]
  (let [document (Document.)]
    (doseq [[key value] map]
      (add-field document key (as-str value) (key (meta map))))
    document))

(defn add
  "Add hash-maps to the search index."
  [index & items]
  (with-open [writer (index-writer index)]
    (doseq [i items]
      (cond
        (map? i) (.addDocument writer
                               (map->document i))))))

(defn delete
  "Deletes hash-maps from the search index."
  [index & maps]
  (with-open [^IndexWriter writer (index-writer index)]
    (doseq [m maps]
      (let [query (BooleanQuery.)]
        (doseq [[key value] m]
          (.add query
                (BooleanClause.
                 (TermQuery. (Term. (.toLowerCase (as-str key))
                                    (.toLowerCase (as-str value))))
                 BooleanClause$Occur/MUST)))
        (.deleteDocuments writer (into-array [query]))))))

(defn- document->map
  "Turn a Document object into a map."
  ([^Document document score]
     (document->map document score (constantly nil)))
  ([^Document document score highlighter]
     (let [m (into {} (for [^Field f (.getFields document)]
                        [(keyword (.name f)) (.stringValue f)]))
           fragments (highlighter m) ; so that we can highlight :_content
           m (dissoc m :_content)]
       (with-meta
         m
         (-> (into {}
                   (for [^Field f (.getFields document)
                         :let [field-type (.fieldType f)]]
                     [(keyword (.name f)) {:stored (.stored field-type)
                                           :tokenized (.tokenized field-type)}]))
             (assoc :_fragments fragments :_score score)
             (dissoc :_content))))))

(defn- make-highlighter
  "Create a highlighter function which will take a map and return highlighted
fragments."
  [^Query query ^IndexSearcher searcher config]
  (if config
    (let [indexReader (.getIndexReader searcher)
          scorer (QueryScorer. (.rewrite query indexReader))
          config (merge {:field :_content
                         :max-fragments 5
                         :separator "..."
                         :pre "<b>"
                         :post "</b>"}
                        config)
          {:keys [field max-fragments separator fragments-key pre post]} config
          highlighter (Highlighter. (SimpleHTMLFormatter. pre post) scorer)]
      (fn [m]
        (let [str (field m)
              token-stream (.tokenStream ^Analyzer *analyzer*
                                         (name field)
                                         (StringReader. str))]
          (.getBestFragments ^Highlighter highlighter
                             ^TokenStream token-stream
                             ^String str
                             (int max-fragments)
                             ^String separator))))
    (constantly nil)))

(defn search
  "Search the supplied index with a query string."
  [index query max-results
   & {:keys [highlight default-field default-operator page results-per-page]
      :or {page 0 results-per-page max-results}}]
  (with-open [reader (index-reader index)]
    (let [default-field (or default-field :_content)
          searcher (doto (IndexSearcher. reader) (.setSimilarity *similarity*))
          parser (doto (QueryParser. (as-str default-field)
                                     *analyzer*)
                   (.setDefaultOperator (case (or default-operator :or)
                                          :and QueryParser/AND_OPERATOR
                                          :or  QueryParser/OR_OPERATOR)))
          query (.parse parser query)
          hits (.search searcher query (int max-results))
          start (* page results-per-page)
          end (min (+ start results-per-page) (.totalHits hits))]
      (doall
       (with-meta (for [hit (map (partial aget (.scoreDocs hits))
                                 (range start end))]
                    (document->map (.doc ^IndexSearcher searcher
                                         (.doc ^ScoreDoc hit))
                                   (.score ^ScoreDoc hit)))
                                        ;highlighter))
         {:_total-hits (.totalHits hits)
          :_max-score (.getMaxScore hits)})))))
