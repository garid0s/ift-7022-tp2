(ns ift-7022-tp2.stats
  (:require [clojure.set :as set]
            [incanter.stats :as incanter]))

(defn precision [true-positive-count selected-element-count]
    (/ true-positive-count selected-element-count))

(defn- recall [true-positive-count relevant-element-count]
    (/ true-positive-count relevant-element-count))

(defn- f-measure [precision recall]
  (if (and (zero? precision) (zero? recall))
    0
    (* 2 (/ (* recall precision) (+ recall precision)))))

(defn statistics [expected actual]
  (let [true-positive-count (count (set/intersection (into #{} expected)
                                                     (into #{} actual)))
        relevant-element-count (count expected)
        selected-element-count (count actual)
        p (precision true-positive-count selected-element-count)
        r (recall true-positive-count relevant-element-count)
        f (f-measure p r)]
    {:precision p
     :recall r
     :f-measure f}))

(defn mean-median
  [distribution]
  {:mean (incanter/mean distribution)
   :median (incanter/median distribution)})

(defn- group-distributions-by-name
  [distributions]
  (reduce (fn [m [key value]]
            (assoc m key (conj (get m key []) value)))
          {}
          (apply concat distributions)))

(defn- replace-distribution-by-stats
  [map-of-stats [distribution-name distribution]]
  (assoc map-of-stats
         distribution-name
         (mean-median distribution)))

(defn mean-median-of-distributions
  [distributions]
  (->> distributions
       group-distributions-by-name
       (reduce replace-distribution-by-stats {})))
