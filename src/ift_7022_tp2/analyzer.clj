(ns ift-7022-tp2.analyzer
  (:import java.io.Reader
           org.apache.lucene.analysis.Analyzer
           org.apache.lucene.analysis.standard.StandardAnalyzer
           org.apache.lucene.analysis.Analyzer$TokenStreamComponents org.apache.lucene.analysis.core.StopAnalyzer
           org.apache.lucene.analysis.en.PorterStemFilter
           org.tartarus.snowball.ext.EnglishStemmer
           org.apache.lucene.analysis.Tokenizer
           org.apache.lucene.analysis.core.LowerCaseTokenizer))

(def default-analyzer
  (proxy [Analyzer] []
    (createComponents [^String field-name]
      (let [source (LowerCaseTokenizer.)]
        (Analyzer$TokenStreamComponents. source)))))

(def porter-stemmer-analyzer
  (proxy [Analyzer] []
    (createComponents [^String field-name]
      (let [source (LowerCaseTokenizer.)]
        (Analyzer$TokenStreamComponents. source (PorterStemFilter. source))))))

(def stop-words-analyzer
  (StopAnalyzer. StopAnalyzer/ENGLISH_STOP_WORDS_SET))

(def all-analyzers
  [{:analyzer default-analyzer
    :description (str "Default Analyzer: without stemming "
                      "or stop words. Only to lower case.")}
   {:analyzer porter-stemmer-analyzer
    :description "Porter-Stemming Analyzer"}
   {:analyzer stop-words-analyzer
    :description "English-Stop-Words Analyzer"}])

