(ns ift-7022-tp2.index
  (:require [ift-7022-tp2.lucene-wrapper :as lucene]
            [clojure.java.io :as io]
            [clojure.string :as string]))

;;=======================================================
;; Document collection

(def file-collection-file (io/file (io/resource "file_collection.txt")))
(def line-per-document 7)

(defn- document-number
  [doc-no-string]
  (->> doc-no-string
       (re-matches #"\.DocNo\s(\d+)")
       last
       Integer/parseInt))

(defn- parse-document
  [document-as-seq]
  {:doc-no (document-number (nth document-as-seq 0))
   :doc-key (nth document-as-seq 2)
   :doc-title (nth document-as-seq 4)
   :doc-content (nth document-as-seq 6)})

(defn- file-collection->seq-of-file-map
  [file]
  (->> file
       slurp
       string/split-lines
       (partition line-per-document)
       (map parse-document)))

(defonce documents-to-index (file-collection->seq-of-file-map file-collection-file))

(def document-metadata {:doc-no {:stored true :indexed false}
                        :doc-key {:stored true :indexed false}
                        :doc-title {:stored true :indexed true}
                        :doc-content {:stored false :indexed true}})


;;=======================================================
;; Index documents
(defn- add-documents-to-index!
  [index documents]
  (let [documents-with-meta (map #(with-meta % document-metadata) documents)]
    (apply lucene/add index documents-with-meta)))

(defn build-index
  []
  (let [file-index (lucene/memory-index)]
    (add-documents-to-index! file-index documents-to-index)
    file-index))

;;=======================================================
;; Search index
(def default-number-of-results 50)

(defn search-index
  ([index query]
   (search-index index query default-number-of-results))
  ([index query number-of-results]
   (->> (lucene/search index query number-of-results :default-field :doc-content)
        (map #(get % :doc-key))
        (into []))))
