(ns ift-7022-tp2.query
  (:require [clojure.java.io :as io]
            [clojure.string :as string]))

(def query-value-file (io/file (io/resource "list_query.txt")))
(def query-relevant-docs-file (io/file (io/resource "relevant_docs.txt")))

(defn- extract-regex-from-lines-of-file
  [file regex]
  (->> file
       slurp
       string/split-lines
       (map #(re-matches regex %))
       (map #(subvec % 1))))

(defn- query-value-map
  []
  (into {} 
        (extract-regex-from-lines-of-file
         query-value-file
         #"^(\S+)\t(.+?)\s*")))

(defn- relevant-doc-vectors
  []
  (extract-regex-from-lines-of-file query-relevant-docs-file #"(\S+)\t(\S+).*"))

(defn- relevant-doc-map
  []
  (->> (relevant-doc-vectors)
       (reduce (fn [m k] (assoc m (first k) (conj (get m (first k) []) (second k))))
               {})))

(defrecord Query [id value relevant-documents])

(defn- query-records
  []
  (let [values (query-value-map)
        relevant-docs (relevant-doc-map)]
    (map (fn [[id value]]
           (Query. id value (get relevant-docs id)))
         values)))

(defonce all-test-queries (into [] (query-records)))
