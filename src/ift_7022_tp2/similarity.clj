(ns ift-7022-tp2.similarity
  (:import (org.apache.lucene.search.similarities DefaultSimilarity)))

(def tf-similarity
  (proxy [DefaultSimilarity] []
    (idf [^long _ ^long _]
      1))) ;; Only idf is 1 so it doesn't affect the scoring

(def tf-idf-similarity (DefaultSimilarity.))

(def all-similarities
  [{:similarity tf-similarity
    :description "tf only similarity (with idf = 1)"}
   {:similarity tf-idf-similarity
    :description "tf*idf similarity"}])
