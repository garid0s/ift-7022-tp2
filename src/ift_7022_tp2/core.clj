(ns ift-7022-tp2.core
  (:require [ift-7022-tp2.query :as tp2-query]
            [ift-7022-tp2.analyzer :as tp2-analyzer]
            [ift-7022-tp2.similarity :as tp2-similarity]
            [ift-7022-tp2.index :as tp2-index]
            [ift-7022-tp2.stats :as tp2-stats]
            [ift-7022-tp2.lucene-wrapper :as lucene])
  (:gen-class))

(defn add-results-to-query
  [query index]
  (let [results (tp2-index/search-index
                 index
                 (:value query))]
    (assoc query :results results)))

(defn statistics-of-query
  [query]
  (let [expected (:relevant-documents query)
        actual (:results query)]
    (tp2-stats/statistics expected actual)))

(defn run-tests
  [analyzer similarity]
  (binding [lucene/*analyzer* analyzer
            lucene/*similarity* similarity]
    (let [index (tp2-index/build-index)]
      (->> tp2-query/all-test-queries
           (map #(add-results-to-query % index))
           (map statistics-of-query)
           (tp2-stats/mean-median-of-distributions)))))

(def result-file "test-results.csv")

(defn -main
  []
  (println "=============================================================")
  (println "=============================================================")
  (println "=============        RUNNING TESTS             ==============")
  (println "=============================================================")
  (println "=============================================================")
  (spit result-file "test,mean precision,median precision,mean recall,median recall,mean f-measure,median f-measure\n")
  (doseq [analyzer tp2-analyzer/all-analyzers
          similarity tp2-similarity/all-similarities]
    (println "-----------------------------------------------")
    (println "***********************************************")
    (println "-----------------------------------------------")
    (println (format "Using analyzer: \t%s" (:description analyzer)))
    (println (format "Using similarity:\t%s" (:description similarity)))

    (let [test-results (run-tests (:analyzer analyzer)
                                  (:similarity similarity))]
      (println "Precision:\t" (:precision test-results))
      (println "Recall:\t\t" (:recall test-results))
      (println "F-measure:\t" (:f-measure test-results))
      (spit result-file (format "%s %s,%f,%f,%f,%f,%f,%f\n"
                                (:description analyzer)
                                (:description similarity)
                                (get-in test-results [:precision :mean])
                                (get-in test-results [:precision :median])
                                (get-in test-results [:recall :mean])
                                (get-in test-results [:recall :median])
                                (get-in test-results [:f-measure :mean])
                                (get-in test-results [:f-measure :median]))
            :append true))))
